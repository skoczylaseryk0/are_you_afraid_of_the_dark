name := "are_you_afraid_of_the_dark"

version := "0.1"

scalaVersion := "2.13.3"


libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"

libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0"