package io.scalac

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class PhotoManagerPositiveTest extends AnyFlatSpec with Matchers {
  private val photoManager: PhotoManager = new PhotoManager

  behavior of "Photo manager"

  it should "read files only with jpg and png extension" in {
    val redImagesList = photoManager.readAllPhotos()

    val filteredRedImagesList = redImagesList.count(file => !file.extension.equals("png") || !file.extension.equals("jpg"))

    redImagesList.size shouldBe filteredRedImagesList
  }
}
