package io.scalac

import java.io.File
import javax.imageio.ImageIO
import org.scalatest.AppendedClues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.collection.mutable.ListBuffer

class PhotoManagerNegativeTest extends AnyFlatSpec with AppendedClues with Matchers with PathHelper {
  private val photoManager: PhotoManager = new PhotoManager

  behavior of "Photo manager"

  it should "not read any file if directory is empty" in {

    val files = inDirectory.listFiles
    val imagesList = readAllPhotosByDotAndNoFilters(files)

    files.foreach(file => file.delete())

    try {
      photoManager.readAllPhotos()
      fail()
    } catch {
      case _: IllegalArgumentException =>
    } finally {
      if (files.nonEmpty) {
        for (x <- 0 until files.size) {
          ImageIO.write(imagesList(x).bufferedImage, imagesList(x).extension, new File(files(x).getPath))
        }
      }
    }
  }

  List("jpeg", "txt", "doc", "docx", "pdf", "gif", "svg").foreach { ext =>
    it should s"not read files with $ext extension" in {
      new File(s"${inPath}test$ext.$ext").createNewFile()

      photoManager.readAllPhotos().count(image => image.extension.equals(ext)) shouldBe 0 withClue s"File with $ext was red"

      new File(s"${inPath}test$ext.$ext").delete()
    }
  }

  it should "return exception when directory not exist" in {

    val files = inDirectory.listFiles

    val imagesList = readAllPhotosByDotAndNoFilters(files)

    files.foreach(file => file.delete())
    new File(inPath).delete()

    try {
      photoManager.readAllPhotos()
      fail()
    } catch {
      case _: NullPointerException =>
    }

    if (files.nonEmpty) {
      new File(inPath).mkdir()
      for (x <- 0 until files.size) {
        imagesList(x)
        ImageIO.write(imagesList(x).bufferedImage, imagesList(x).extension, new File(files(x).getPath))
      }
    }
  }

  private def readAllPhotosByDotAndNoFilters(files: Array[File]): ListBuffer[ImageWithExtension] = {
    var imagesList = ListBuffer[ImageWithExtension]()

    files.foreach { file =>
      val extension: String = file.getPath.split("\\.")(1)
      val image = ImageIO.read(file)
      imagesList += ImageWithExtension(image, extension, None)
    }
    imagesList
  }
}

