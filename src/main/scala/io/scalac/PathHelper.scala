package io.scalac

import java.io.File

trait PathHelper {
  val slash: String = File.separator
  val canonicalPath: String = new File(".").getCanonicalPath + slash
  val outPath: String = canonicalPath + s"src${slash}main${slash}scala${slash}io${slash}scalac${slash}config${slash}out${slash}"
  val inPath: String = canonicalPath + s"src${slash}main${slash}scala${slash}io${slash}scalac${slash}config${slash}in${slash}"
  val outDirectory: File = new File(outPath)
  val inDirectory: File = new File(inPath)

  def getPathToWriteImage(image: ImageWithExtension, cutOffPoint: Int): String = {
    val ext = image.extension match {
      case ext if ext.equals("jpg") => "jpg"
      case ext if ext.equals("png") => "png"
      case _ => throw new IllegalArgumentException(s"Incorrect extension")
    }

    val name = image.darknesPoints.get match {
      case points if points == 100 => s"${outPath}perfect_black_dark_$points"
      case points if points >= cutOffPoint && points < 100 => s"${outPath}colorful_image_dark_$points"
      case points if points > 0 && points < cutOffPoint => s"${outPath}colorful_image_bright_$points"
      case points if points == 0 => s"${outPath}perfect_white_bright_$points"
    }

    s"$name(${checkHowManyFilesExistWithThisName(name)}).$ext"
  }

  private def checkHowManyFilesExistWithThisName(name: String): Int = {
    outDirectory.listFiles().map(file => file.getPath).count(path => path.contains(name + "(") || path.contains(name + ".")) + 1

  }
}
