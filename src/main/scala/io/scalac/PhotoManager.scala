package io.scalac

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ListBuffer
import scala.collection.parallel.ParSeq


class PhotoManager extends PathHelper {

  def readAllPhotos(): ListBuffer[ImageWithExtension] = {
    var imagesList = ListBuffer[ImageWithExtension]()
    val files = inDirectory
      .listFiles

    if (files.isEmpty || files == None) {
      throw new IllegalArgumentException("No directory or photos in directory to read")
    }
    files.filter(file => file.getName.endsWith(".jpg") || file.getName.endsWith(".png"))
      .foreach { file =>
        val extension: String = file.getPath.substring(file.getPath.length - 3, file.getPath.length)
        val image = ImageIO.read(file)
        imagesList += ImageWithExtension(image, extension, None)
      }
    imagesList
  }

  def writeAllPhotos(allCorrectPhotos: ParSeq[ImageWithExtension], cutOffPoint: Int): Unit = {
    val file = new File(outPath)
    if (!file.exists())
      file.mkdir()

    allCorrectPhotos.foreach {
      image => writePhoto(image.copy(darknesPoints = Some(new DarknessMeter(image.bufferedImage).call())), cutOffPoint)
    }
  }

  private def writePhoto(image: ImageWithExtension, cutOffPoint: Int): Unit = {
    val name = getPathToWriteImage(image, cutOffPoint)
    ImageIO.write(image.bufferedImage, image.extension, new File(name))
  }
}

case class ImageWithExtension(bufferedImage: BufferedImage, extension: String, darknesPoints: Option[Int]) {}