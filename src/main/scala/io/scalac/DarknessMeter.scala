package io.scalac

import java.awt.image.BufferedImage
import java.util.concurrent.Callable

class DarknessMeter(bufferedImage: BufferedImage) extends Callable[Int] {

  override def call(): Int = {
    measureDarkness(bufferedImage)
  }

  private def measureDarkness(bufferedImage: BufferedImage): Int = {
    var pixelCount = 0
    var darkPixels = 0

    for (coordinateX <- 0 until bufferedImage.getWidth) {
      for (coordinateY <- 0 until bufferedImage.getHeight) {
        pixelCount += 1
        val rgb = bufferedImage.getRGB(coordinateX, coordinateY)
        val red = (rgb >> 16) & 0x000000FF
        val green = (rgb >> 8) & 0x000000FF
        val blue = rgb & 0x000000FF
        if (red + green + blue < 40) darkPixels += 1
      }
    }
    ((darkPixels.asInstanceOf[Float] / pixelCount.asInstanceOf[Float]) * 100).toInt
  }
}
