package io.scalac

import scala.collection.parallel.CollectionConverters._


object App {

  def main(args: Array[String]): Unit = {
    val photoManager = new PhotoManager
    photoManager.writeAllPhotos(photoManager.readAllPhotos().par, 55)
  }
}
