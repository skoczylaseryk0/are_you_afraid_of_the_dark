Measuring darkness




Important notes:
1. Two of twenty-six red images was recognized correctly in scale from 0 to 100.
2. cutOffPoint is the name of variable in class App when you can set you cut-off value.
3. Measuring darkness work correctly for 24 of 26 red images so corectness is about 93%.
4. I didn't read all files because 2 of files are with .jpeg extension.
   I don't know it is your mistake or conscious activity but in description of my task I see "read all jpg and png files".
5. I decided to use multithreading to measure darkness of red photos because it is twice as fast as without multithreading
   (average multithreading execution time= 1.47, average standard execution time =  0.60).
6. Numeration of files was added by me so you can run my program more than one time and Files with same path don't replace.
   They will be something like that:  colorful_image_bright_(1).jpg, colorful_image_bright_(2).png


Description:
There are 4 classes in my app and two folders (in and out) as you wished. Class App is class to run.
DarknessMeter class extends Callable to measure dark faster which I mentioned in "Important Notes".
I created PathHelper trait to get paths and directories in my PhotoManager where I read and write images.
I also implemented some tests. I know that is very small number of test coverage of my project.
I had no time to do all test cases. I tested my project on 150 other images and everything went well with reading, writing etc.

Run:
1.If you want to run my app please write command run in sbt schell.
2. If you want to run tests please use testOnly *NameOfClass.





